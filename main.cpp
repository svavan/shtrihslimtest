/*
Shtrih-M, test POS2 protocol 
Moscow, 2015
*/

/***************************************************************************
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation version 2 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program; if not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
***************************************************************************/

#include <stdio.h>
#include <time.h>

#include "pos2.h"

#include "rs232.h"

// device metrics
static char         dm_power_of_ten;
static unsigned int dm_max;
static unsigned int dm_min;
static unsigned int dm_tare;
static unsigned int dm_range1;
static unsigned int dm_range2;
static unsigned int dm_range3;
static unsigned int dm_disk1;
static unsigned int dm_disk2;
static unsigned int dm_disk3;
static unsigned int dm_disk4;
static char         dm_div;
static char         dm_mult;
// device state
static unsigned int ds_state;
static long         ds_weight;
static unsigned int ds_tare;


// port openinig
int openPort(char *portname, int baud)
{
    char mode[] = {'8', 'N', '1', 0};
    RS232_SetZeroName(portname);
    if(RS232_OpenComport(0, baud, mode) == 0) return 1;  // ok
    return 0; // error
};

//
int readAnswer(void)
{
    clock_t start;
    start = clock();
    // waiting for answer for a secod only
    while ((clock() - start) < (CLOCKS_PER_SEC * 3))
    {
        unsigned char buf[1];
        int n = RS232_PollComport(0, buf, 1);
        if (n)
        {
            pos2_receive_byte(buf[0]);
            if (pos2_is_received()) return 1;
        };
    };
    return 0;
};

// "Get Device" command
int sendGetDeviceParamCmd(void)
{
    pos2_clear();
    pos2_add_byte(0xFC);
    pos2_add_crc();
    if (RS232_SendBuf(0, pos2_get_packet_pointer(), 4) > 3) return 1;
    return 0;
};

//
int readGetDeviceParamAns(void)
{
    if (readAnswer())
    {
        if (pos2_check_received_answer(0xFC))
        {
            printf("- Found device:   %.33s\n", pos2_ger_r_buf_pointer(10));
            return 1;
        };
    }
    printf("- No answer");
    return 0;
};

// "Get weight channel properties"
int sendGetChannelPropertiesCmd(unsigned char channel)
{
    pos2_clear();
    pos2_add_byte(0xE8);
    pos2_add_byte(channel);
    pos2_add_crc();
    if (RS232_SendBuf(0, pos2_get_packet_pointer(), 5) > 4) return 1;
    return 0;
};

double convertToKg(int val)
{
    double result;
    result = val;
    switch (dm_power_of_ten)
    {
        case (-4): result = result / 10000; break;
        case (-3): result = result / 1000;  break;
        case (-2): result = result / 100;   break;
    };
    return result;
};

double convertToG(int val)
{
    double result;
    result = val;
    switch (dm_power_of_ten)
    {
        case (-4): result = result / 10; break;
        case (-3): result = result * 1;  break;
        case (-2): result = result * 10; break;
    };
    return result;
};

int readGetWeightChannelProperties(void)
{
    if (readAnswer())
    {
        if (pos2_check_received_answer(0xE8))
        {
            dm_power_of_ten = pos2_get_received_byte(7);
            dm_max = pos2_get_received_byte(8) + pos2_get_received_byte(9)*0x100;
            dm_range1 = pos2_get_received_byte(14) + pos2_get_received_byte(15)*0x100;
            dm_range2 = pos2_get_received_byte(16) + pos2_get_received_byte(17)*0x100;
            dm_range3 = pos2_get_received_byte(18) + pos2_get_received_byte(19)*0x100;
            printf("- Device metrics: Max ");
            if (dm_range1)
            {
                printf("%.3g/", convertToKg(dm_range1));
                if (dm_range2)
                {
                    printf("%.3g/", convertToKg(dm_range2));
                    if (dm_range3)
                    {
                        printf("%.3g/", convertToKg(dm_range3));
                    };
                };
            };
            printf("%.3gkg  ", convertToKg(dm_max));
            dm_min = pos2_get_received_byte(10) + pos2_get_received_byte(11)*0x100;
            printf("Min %.3gg  ", convertToG(dm_min));
            printf("e=");
            dm_disk1 = pos2_get_received_byte(20);
            dm_disk2 = pos2_get_received_byte(21);
            dm_disk3 = pos2_get_received_byte(22);
            dm_disk4 = pos2_get_received_byte(23);
            printf("%.3g", convertToG(dm_disk1));
            if (dm_disk2)
            {
                printf("/%.3g", convertToG(dm_disk2));
                if (dm_disk3)
                {
                    printf("/%.3g", convertToG(dm_disk3));
                    if (dm_disk4)
                    {
                        printf("/%.3g", convertToG(dm_disk4));
                    };
                };
            };
            printf("g  ");
            dm_tare = pos2_get_received_byte(12) + pos2_get_received_byte(13)*0x100;
            printf("T=%.3gkg\n", -convertToKg(dm_tare));
            return 1;
        };
    }
    printf("- No answer");
    return 0;
};

// "Get weight channel state"
int sendGetChannelStateCmd(void)
{
    pos2_clear();
    pos2_add_byte(0x3A);
    pos2_add_password(0);
    pos2_add_crc();
    if (RS232_SendBuf(0, pos2_get_packet_pointer(), 8) > 7) return 1;
    return 0;
};

int readGetWeightChannelState(void)
{
    if (readAnswer())
    {
        if (pos2_check_received_answer(0x3A))
        {
            printf("- Device state:   ");
            ds_state = pos2_get_received_byte(4) + pos2_get_received_byte(5)*0x100;
            if (ds_state & (1 << 9))
            {
                printf("a hardware error");
            }
            else if (ds_state & (1 << 8))
            {
                printf("error: device underload");
            }
            else if (ds_state & (1 << 6))
            {
                printf("error: device overload");
            }
            else if (ds_state & (1 << 7))
            {
                printf("calibration error");
            }
            else if (ds_state & (1 << 5))
            {
                printf("initial zero error. Please, unload platform.");
            }
            else
            {
                if (ds_state & (1 << 1)) printf("Z "); else printf("  ");
                if (ds_state & (1 << 3)) printf("T "); else printf("  ");
                if (ds_state & (1 << 4)) printf("E "); else printf("  ");
                if (ds_state & (1 << 0)) printf("F "); else printf("  ");
            };
            union {long l; unsigned char b[4]; } t;
            t.b[0] = pos2_get_received_byte(6);
            t.b[1] = pos2_get_received_byte(7);
            t.b[2] = pos2_get_received_byte(8);
            t.b[3] = pos2_get_received_byte(9);
            ds_weight = t.l;
            ds_tare = pos2_get_received_byte(10) + pos2_get_received_byte(11)*0x100;
            if (dm_power_of_ten > -4)
            {
                printf(" W=%.3fkg  ", convertToKg(ds_weight));
                printf(" T=%.3fkg", -convertToKg(ds_tare));
            }
            else
            {
                printf(" W=%.4fkg  ", convertToKg(ds_weight));
                printf(" T=%.4fkg", -convertToKg(ds_tare));
            };
            return 1;
        };
    }
    printf("- No answer");
    return 0;
};



//
int main(int argc, char *argv[])
{
    printf("\n(2015) Test Shtrih-M POS2 weightscale\n");
    printf("Args: port (baud) (u), 'port' is mandatory,");
    printf(" 'u' is for unlimited state asking.\n");
    if (argc > 1)
    {
        int baud;
        baud = 9600;
        if (argc > 2)
        {
            int b = atoi(argv[2]);
            if (b) baud = b;
        };
        printf("Connection port: %s, baud: %d\n", argv[1], baud);
        if (openPort(argv[1], baud))
        {
            printf("- Port is opened successfully.\n");
            if (sendGetDeviceParamCmd())
            {
                printf("- GetDeviceParam command sent successfully.\n");
                if (readGetDeviceParamAns())
                {
                    if (sendGetChannelPropertiesCmd(0))
                    {
                        printf("- GetChannelProperties command sent successfully.\n");
                        if (readGetWeightChannelProperties())
                        {
                            if (sendGetChannelStateCmd())
                            {
                                printf("- GetChannelState command sent successfully.\n");
                                if (readGetWeightChannelState() &&
                                    argc > 3 && argv[3][0] == 'u' )
                                {
                                    char c;
                                    c = 1;
                                    while (c)
                                    {
                                        clock_t start;
                                        start = clock();
                                        while ((clock() - start) < (CLOCKS_PER_SEC/4)) { ; };

                                        if (sendGetChannelStateCmd())
                                        {
                                            printf("\r");
                                            if (!readGetWeightChannelState()) c = 0;
                                            printf("       ");
                                        }
                                        else c = 0;
                                    };
                                };
                            };
                        };
                    };
                };
            }
            else
            {
                printf("Error sending GetDeviceParam command. \n");
            };
        }
        else
        {
            printf("Error port opening.\n");
        };
        RS232_CloseComport(0);
    }
    else
    {
        printf(".. please, choose the communication port");
    };

    printf("\n\n");
#ifdef _WIN32
    system("PAUSE");
#endif

    return 0;
}
