/*
Shtrih-M, test POS2 protocol
Moscow, 2015
*/

/***************************************************************************
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation version 2 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program; if not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
***************************************************************************/


/*******************************************
                   P O S 2
*******************************************/

#ifndef POS2_H
#define POS2_H

#define  PB_ENQ              0x05
#define  PB_STX              0x02
#define  PB_ACK              0x06
#define  PB_NAK              0x15
#define  PB_BUSY             0x0B
#define  POS2_BUF_LENGTH     (255 - 4)

typedef struct
{
    unsigned char stx;
    unsigned char length;
    unsigned char buf[POS2_BUF_LENGTH];
} st_pos2;

typedef union
{
    st_pos2       fields;
    unsigned char packet[sizeof(st_pos2)];
} un_pos2;

//
unsigned char * pos2_get_packet_pointer(void);
//
unsigned char * pos2_ger_r_buf_pointer(unsigned int pos); 
//
unsigned char   pos2_get_received_byte(unsigned int pos);
//
unsigned char   pos2_check_received_answer(unsigned char cmd);
//  
void            pos2_clear();
//
void            pos2_add_byte(unsigned char byte);
//
void            pos2_add_password(unsigned long p);
//
void            pos2_add_crc();
//
void            pos2_receive_byte(unsigned char byte);
//
int             pos2_is_received();


#endif // POS2_H
