/*
Shtrih-M, test POS2 protocol 
Moscow, 2015
*/

/***************************************************************************
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation version 2 of the License.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License along
* with this program; if not, write to the Free Software Foundation, Inc.,
* 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*
***************************************************************************/


/*******************************************
                   P O S 2
*******************************************/

#include "pos2.h"

static un_pos2        pos2_buf;
static unsigned char  pos2_r_buf[POS2_BUF_LENGTH];
static unsigned int   pos2_r_buf_p;
static unsigned char  pos2_send_command_state;  


//
unsigned char * pos2_get_packet_pointer(void)
{
  return pos2_buf.packet;
};

//
unsigned char * pos2_ger_r_buf_pointer(unsigned int pos)
{
  return &pos2_r_buf[pos];
};  
  
//
unsigned char pos2_get_received_byte(unsigned int pos)
{
  if (pos < pos2_r_buf_p) return pos2_r_buf[pos];
  return 0;
};  

// 
unsigned char pos2_check_received_answer(unsigned char cmd)
{
  // check if command byte in answer is eqv cmd and error byte is 0   
  if (pos2_r_buf[2] == cmd && pos2_r_buf[3] == 0) 
    return 1;
  return 0;
};
  
//  
void pos2_clear()
{
  unsigned int c;
  for (c = 0; c < sizeof(st_pos2); c++) pos2_buf.packet[c] = 0;
  pos2_buf.fields.stx = PB_STX;
  for (c = 0; c < POS2_BUF_LENGTH; c++) pos2_r_buf[c] = 0;
  pos2_r_buf_p = 0;
}

//
void pos2_add_byte(unsigned char byte)
{
  if (pos2_buf.fields.length < POS2_BUF_LENGTH)
  {
    pos2_buf.fields.buf[pos2_buf.fields.length] = byte;
    pos2_buf.fields.length++;
  }
}

//
void pos2_add_password(unsigned long p)
{
  union
  {
       unsigned char b[4];
       unsigned long l;
  } t;
  t.l = p;
  pos2_add_byte(t.b[0]);
  pos2_add_byte(t.b[1]);
  pos2_add_byte(t.b[2]);
  pos2_add_byte(t.b[3]);
};

//
void pos2_add_crc()
{
  unsigned char c;
  unsigned char crc;
  crc = 0;
  for (c = 1; c < pos2_buf.fields.length + 2; c++)
    crc = crc ^ pos2_buf.packet[c];
  pos2_buf.fields.buf[pos2_buf.fields.length] = crc;
}

//
void pos2_receive_byte(unsigned char byte)
{
  // add byte to buffer
  if (pos2_r_buf_p >= POS2_BUF_LENGTH) return;
  pos2_r_buf[pos2_r_buf_p] = byte;
  pos2_r_buf_p++;
};

//
int pos2_is_received()
{
  // analize STX
  if (pos2_r_buf[0] != PB_STX)
  {
    pos2_r_buf_p = 0;
    return 0;
  };
  // analize LEN
  unsigned char len;
  len = pos2_r_buf[1];
  if (len >= POS2_BUF_LENGTH)
  {
    pos2_r_buf_p = 0;
    return 0;
  };
  // analize pos2_r_buf_p
  if (pos2_r_buf_p > (len + 2))
  {
    // check crc
    unsigned char c;
    unsigned char crc;
    crc = 0;
    for (c = 1; c < len + 2; c++)
      crc = crc ^ pos2_r_buf[c];
    if (crc == pos2_r_buf[len + 2]) return 1;
    else
    {
      pos2_r_buf_p = 0;
      return 0;
    };
  };
  return 0;
};

